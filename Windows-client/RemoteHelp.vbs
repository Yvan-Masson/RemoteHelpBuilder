'    Remote Help Builder
'    Copyright (C) 2017-2019 Yvan Masson <https://masson-informatique.fr>
'
'    This program is free software: you can redistribute it and/or modify
'    it under the terms of the GNU General Public License as published by
'    the Free Software Foundation, either version 3 of the License, or
'    any later version.
'    
'    This program is distributed in the hope that it will be useful,
'    but WITHOUT ANY WARRANTY; without even the implied warranty of
'    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'    GNU General Public License for more details.
'    
'    You should have received a copy of the GNU General Public License
'    along with this program.  If not, see <http://www.gnu.org/licenses/>.

' intermediate server FQDN or IP address
server="help.my-company.com"
' intermediate server's SSH port
ssh_port="80"
' SSH public key of the server (beware of the triple quotes)
hostkey="""ssh-ed25519 256 e5:32:8f:c3:fb:c6:e3:6e:8a:45:78:dc:d8:5c:84:03"""


' title that will be displayed on all the windows
windows_title="Help by My Company"
' first message show to the user
initial_message="To get help:" & vbCrlf & _
"- give me a phone call so that I give you a password" & vbCrlf & _
"- type this password in the box" & vbCrlf & _
"- click on OK to initiate connection"
' string displayed when waiting for SSH connection
connecting="Connecting..."
' string displayed when SSH server denies connection
wrong_password="Could not connect to " & server & ": incorrect password?"
' string displayed when SSH connection fails without log (should not happen)
unknown_ssh_error="Could not connect to " & server & ". Please try again."
' string displayed when SSH fails but returning an error message (the error
' message will be displayed on a new line after this)
ssh_error="Could not connect to " & server & ":"
' string displayed when starting VNC server
vnc_start="Starting remote control software..."
' string displayed when UltraVNC fails to start
vnc_error="Could not start remote control software, aborting."
' string displayed when help is ongoing
help_ongoing="Connection established." & _
"Once assistance is finished please click ""OK"" to close the connection."

' time to wait for SSH connection (milliseconds)
ssh_delay=13000
' time to wait for VNC startup (milliseconds)
vnc_delay=3000

' you should not need to change this
ssh_user="remote-help"

'''''''''''
' Functions
'''''''''''

' display msgbox in another process
' Written by Denis St-Pierre
Function ProgressMsg( strMessage, strWindowTitle )
    Set wshShell = WScript.CreateObject( "WScript.Shell" )
    strTEMP = wshShell.ExpandEnvironmentStrings( "%TEMP%" )
    If strMessage = "" Then
        On Error Resume Next
        objProgressMsg.Terminate( )
        On Error Goto 0
        Exit Function
    End If
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    strTempVBS = strTEMP + "\" & "tmp-message.vbs"

    Set objTempMessage = objFSO.CreateTextFile( strTempVBS, True )
    objTempMessage.WriteLine( "MsgBox""" & strMessage & """, 4096, """ & strWindowTitle & """" )
    objTempMessage.Close

    On Error Resume Next
    objProgressMsg.Terminate( )
    On Error Goto 0

    Set objProgressMsg = WshShell.Exec( "%windir%\system32\wscript.exe " & strTempVBS )

    Set wshShell = Nothing
    Set objFSO   = Nothing
End Function

' Function to check if a process is running
function isProcessRunning(strComputer, strProcess)
    Dim Process, strObject
    IsProcessRunning = False
    strObject = "winmgmts://" & strComputer
    For Each Process in GetObject( strObject ).InstancesOf("Win32_process")
    If UCase(Process.name) = UCase(strProcess) Then
        IsProcessRunning = True
        Exit Function
    End If
    Next
    IsProcessRunning = False
end function


'''''''''''''
' Main script
'''''''''''''

Dim ObjProgressMsg

' get program path
strPath = Wscript.ScriptFullName
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(strPath)
path = objFSO.GetParentFolderName(objFile)

' get user temp folder
Set oShell = CreateObject( "WScript.Shell" )
user_temp=oShell.ExpandEnvironmentStrings("%TEMP%")

passwd=inputbox(initial_message, windows_title)
              
if passwd = "" Then Wscript.Quit

' kill old processes that could remain
Dim WshShell : Set WshShell = CreateObject("WScript.Shell")
WshShell.Run "taskkill /F /im plink.exe", 0, true
WshShell.Run "taskkill /F /im uvnc.exe", 0, true

' start ssh
WSHShell.run "cmd /c """""&path&"\PuTTY\plink.exe"" -batch -l " & ssh_user & " " & _
             "-pw " & passwd & " -T -N -R 5900:localhost:5900 -P " & ssh_port & " " & _
             server & " -hostkey " & hostkey & " " & _
             "> %TEMP%\plink-stdout.log 2> %TEMP%\plink-stderr.log""", 0, false

ProgressMsg connecting, windows_title
Wscript.sleep ssh_delay
ProgressMsg "", windows_title

' if ssh password is denied
InputFile = user_temp&"\plink-stderr.log"
Set FSO = CreateObject("Scripting.FileSystemObject")
Set rdFile = FSO.OpenTextFile(InputFile, 1)
strContents = rdFile.ReadAll
rdFile.Close

if InStr(1, strContents, "Access Denied", 1) > 0 then
    WshShell.Run "taskkill /F /im plink.exe", 0, False
    msgbox wrong_password, vbOkOnly, windows_title
    FSO.DeleteFile InputFile
    Wscript.Quit
end if

' eventually display ssh error
if not isProcessRunning(".","plink.exe") then
    InputFile = user_temp&"\plink-stderr.log"
    Set FSO = CreateObject("Scripting.FileSystemObject")
    set objFile = FSO.GetFile(InputFile)

    if objFile.size = "0" then
        msgbox unknown_ssh_error, vbOKOnly, windows_title
    else
        Set oFile = FSO.OpenTextFile(InputFile)
        errorlog = oFile.ReadAll
        oFile.Close

        msgbox ssh_error & vbCrlf & vbCrlf & errorlog, vbOKOnly, windows_title
    end if
    WshShell.Run "taskkill /F /im plink.exe", 0, False

    Wscript.Quit
end if

' start UltraVNC
WSHShell.run """"&path&"\UltraVNC\winvnc.exe"" -run", 0, false

ProgressMsg vnc_start, windows_title
Wscript.sleep vnc_delay
ProgressMsg "", windows_title

' eventually display UltraVNC error
if not isProcessRunning(".","winvnc.exe") then
    msgbox vnc_error, vbOKOnly, windows_title
    WshShell.Run "taskkill /F /im plink.exe", 0, False
    Wscript.Quit
end if

msgbox help_ongoing, vbOKOnly, windows_title

' finally kill processes
WshShell.Run "taskkill /F /im plink.exe", 0, False
WshShell.Run "taskkill /F /im winvnc.exe", 0, False
