; See http://www.jrsoftware.org/ishelp/index.php for documentation

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{396DB1FD-DA50-4D34-9EA2-E8217E3AB7E8}
AppName=My Remote Help Tool
AppVersion=2.1
AppPublisher=My Name
; Optional
AppPublisherURL=https://my-website.com
; Will install by default in "%PROGRAMFILES (X86)%" on Windows 64bits 
DefaultDirName={pf32}\{#SetupSetting("AppName")}
; The name of your icon
#define IconFile "My-icon.ico"
; The end of the following value will append version to the generated installer
OutputBaseFilename=my-remote-help-tool_{#SetupSetting("AppVersion")}
; You should not need to modify those variables
SourceDir=Sources\
OutputDir=..\Output\
SetupIconFile={#IconFile}
DisableProgramGroupPage=yes
LicenseFile=License.txt
Compression=lzma
SolidCompression=yes

[Languages]
; Leave only one language if you don't want the installer to ask which lang to use
Name: "en"; MessagesFile: "compiler:Default.isl"
Name: "fr"; MessagesFile: "compiler:Languages\French.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[Files]
Source: "RemoteHelp.vbs"; DestDir: "{app}"; Flags: ignoreversion
Source: "Changelog.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "License.txt"; DestDir: "{app}"; Flags: ignoreversion
Source: "{#IconFile}"; DestDir: "{app}"; Flags: ignoreversion
Source: "PuTTY\*"; DestDir: "{app}\PuTTY"; Flags: ignoreversion
Source: "UltraVNC\*"; DestDir: "{app}\UltraVNC"; Flags: ignoreversion

[Icons]
Name: "{commonprograms}\{#SetupSetting("AppName")}"; Filename: "{app}\RemoteHelp.vbs"; IconFilename: "{app}\{#IconFile}"
Name: "{commondesktop}\{#SetupSetting("AppName")}"; Filename: "{app}\RemoteHelp.vbs"; IconFilename: "{app}\{#IconFile}"; Tasks: desktopicon

