**While functionnal, this project has been abandonned: you should have a look
on [MeshCentral](https://www.meshcommander.com/meshcentral2), which is
undoubtely a far better solution.**


# RemoteHelpBuilder

RemoteHelpBuilder aims at providing technical people guidelines to build a free
(libre), on-demand system, to help non-technical remote people. It can be seen
as a basic alternative to TeamViewer© (with some [limitations](#overview)), but
based on VNC and SSH existing implementations.

You need to set it up once (configuring the "_intermediate server_", customizing
and packaging a little "_client software_" intended for the "_helped_"), and you
can then easily provide help to non-tech people running Windows (≥ XP) or Linux
(currently Debian ≥ 8, Ubuntu ≥ 16.04 and Linux Mint ≥ 18.0).

If you use this software, please send an email: I would be glad to know it is
iseful :-).


# Table of contents

- [Overview](#overview)
- [Prior warning](#prior-warning)
- [First step](#first-step)
- [Configuring the intermediate server](#configuring-the-intermediate-server)
    - [OpenSSH](#openssh)
    - [VNC client and helper script](#vnc-client-and-helper-script)
    - [Server's SSH key](#servers-ssh-key)
- [Client's versionning scheme](#clients-versionning-scheme)
- [Common clients prerequisites](#common-clients-prerequisites)
- [Linux client](#linux-client)
    - [Prepare the "upstream" release](#prepare-the-upstream-release)
        - [Prerequisites](#prerequisites)
        - [Software icon](#software-icon)
        - [Desktop entry](#desktop-entry)
        - [Software, strings and parameters](#software-strings-and-parameters)
        - [Intermediate server's SSH public key](#intermediate-servers-ssh-public-key)
    - [First build of the Debian and derivatives package](#first-build-of-the-debian-and-derivatives-package)
    - [Update the Debian and derivatives package](#update-the-debian-and-derivatives-package)
        - [When you need to fix packaging](#when-you-need-to-fix-packaging)
        - [For everything else](#for-everything-else)
- [Building the Windows client](#windows-client)
    - [First Build](#first-build)
        - [Prerequisites](#prerequisites-1)
        - [PuTTY](#putty)
        - [UltraVNC](#ultravnc)
        - [Wrapper script and variables](#wrapper-script-and-variables)
        - [license](#license)
        - [Software icon](#software-icon-1)
        - [Building with InnoSetup](#building-with-innosetup)
    - [Building a new client version](#building-a-new-client-version)
- [Usage](#usage)
- [Release policy](#release-policy)
- [Frequently asked questions](#frequently-asked-questions)
    - [Debugging](#debugging)
        - [On the intermediate server](#on-the-intermediate-server)
        - [On the Linux client](#on-the-linux-client)
        - [On the Windows client](#on-the-windows-client)
    - [Icon is not displayed when running the Linux client](#icon-is-not-displayed-when-running-the-linux-client)
    - [How to run the VNC client on your computer and not on the server](#how-to-run-the-vnc-client-on-your-computer-and-not-on-the-server)
- [License](#license)


# Overview

Once you have configured the system, here is how it works:

1. Get in touch with the helped (usually by phone) to help him to get and
   install the client software. Even computer beginners should be able to
   install it.
2. Connect to the intermediate server with SSH and X11 forwarding to generate a
   one-time password, and give it to the helped.
3. The helped starts the client software, types the password and validates: this
   initiates SSH port forwarding to the intermediate server and starts a
   VNC server.
3. Start the VNC client on the intermediate server and provide help!
4. When done, disable the one-time password and log out from the intermediate
   server.

Requirements and limitations:

- SSH must not be blocked by a firewall somewhere in the path (SSH port number
  can be chosen), so it will not work if the helped is behind a firewall that
  does
  [deep packet inspection](https://fr.wikipedia.org/wiki/Deep_packet_inspection).
- This system does not allow helping more than one person at the same time.
- You must be confident with the Linux command line and system administration to
  be able to build the system and to understand the security implications
  (hopefully the instructions are clear enough).
- You needs an intermediate server, with root access, that can be joined on its
  SSH port from the Internet (it can be a hosted VPS or a Raspberry Pi behind a
  personnal modem/router). It needs either a static IP address or to be pointed
  to by DNS. You could eventually use your own Linux computer to act as the
  intermediate server. Only trusted users must have access to this server.
- You must distribute the client software to the helped before first use.
- You and the helped need a bare minimum of 1 Mbit/s ADSL connection, but
  2 Mbit/s is advised.
- Wayland Linux clients are not supported.

Many things can be improved, often easily, so feel free to get in touch and
submit pull requests!


# Prior warning

- The setup described here is based on many things at which I am
  inexperienced, and has not been reviewed yet: it works but might contain
  security issues.
- The Windows client embeds Putty and UltraVNC, so you must update this client
  when security updates are published.


# First step

Download all the files so that you can work easily on them, for example with
Git:

```
$ cd ~ && git clone https://framagit.org/Yvan-Masson/RemoteHelpBuilder.git
```


# Configuring the intermediate server

The instructions here target Debian 9 and OpenSSH. Feel free to adapt these to
your particular usage or to your favourite UNIX OS, but be carefull to keep it
secure!


## OpenSSH

We first create a dedicated user for the SSH connection between the helped and
the intermediate server:

```
# adduser --home /no-home --no-create-home --shell /bin/false --disabled-login remote-help
```

We then need to allow the helped to initiate SSH port forwarding and to prevent
him from doing anything else. Add the following section in
`/etc/ssh/sshd_config`:

```
Match User remote-help
    AllowAgentForwarding no
    AllowTcpForwarding yes
    AuthenticationMethods "password"
    ForceCommand echo 'This account can only be used for VNC port forwarding'
    PermitOpen localhost:5900
    PermitTunnel no
    PermitTTY no
    X11Forwarding no
```

Optionnaly, you can change in the above file the port OpenSSH listens on (some
firewall between the server and the helped may block TCP port 22). Here, we
choose to listen on port 80 that is normally never blocked:

```
Port 80
```

To keep the server setup simple and still secure, the VNC client will be run
directly on the server. Ensure that X11 forwarding is enabled on the server,
either globally or for your specific user, still in `/etc/ssh/sshd_config`:

```
X11Forwarding yes
```

Finally restart OpenSSH to use the new configuration (if you changed the port
number you will also be deconnected):

```
# systemctl restart ssh
```


## VNC client and helper script

You can then install the VNC viewer you prefer, but I personnaly install
xtightvncviewer: it is a bit rough but it does not have many dependencies, so it
is great option for a server:

```
# apt install xtightvncviewer --no-install-recommends
```

We now install on the server an optionnal little script that facilitates :

- Enabling the `remote-help` user with a random 4 digits password.
- Remembering of the `xtightvncviewer` command line to start the VNC client.
- Disabling the `remote-help` user so that the helped can not login anymore on
  your server.

Put this script in `/usr/local/sbin/remote-help-toggle`:

```sh
#!/bin/sh

if ! [ "$(id --user)" = "0" ]; then
    echo "Must be run as root"; exit
fi

the_user="remote-help"

if [ "$1" = "enable" ]; then
    # generates password
    pwd=$(shuf -i 1000-9999 -n 1)
    # encrypt it for /etc/shadow
    encpwd=$(echo "$pwd" | mkpasswd --method=sha-512 --stdin)
    usermod --password "$encpwd" "$the_user"
    echo "Password : $pwd"
    echo "To start VNC viewer, from server's network (uncompressed):"
    echo "  xtightvncviewer -encodings \"tight\" -compresslevel 1 -quality 0 localhost"
    echo "To start VNC viewer from another network (compressed):"
    echo "  xtightvncviewer -encodings \"tight\" -compresslevel 9 -quality 0 localhost"
    echo "Use password \"foo\" for Windows computers."

elif [ "$1" = "disable" ]; then
    passwd --lock "$the_user" && echo "Password login for user \"$the_user\" disabled"

else
    echo "Usage : $0 enable|disable"
fi

```

And make sure that it is executable:

```
# chmod 755 /usr/local/sbin/remote-help-toggle
```

The above script uses `mkpasswd` command, so we need to install `whois` package
that provides it:

```
# apt install whois
```


## Server's SSH key

As always, making things securely is a complicated task. Even if the helped and
you will be able to mutually authenticate by a phone call when transmitting the
one-time password, it can be a good thing for the client software to also
authenticate the SSH server with its public key.

Your server probably provides different signature algorithms, and corresponding
public and private keys are stored in `/etc/ssh/ssh_host_*` files. To simplify
the setup, we will pick only one, ed25519. As I understand it, this is a purely
subjective choice, feel free to choose another one (there does not seem to be
good or bad choice, see for example
[this stackexchange thread](https://security.stackexchange.com/questions/50878/ecdsa-vs-ecdh-vs-ed25519-vs-curve25519)).
However, ed25519 public key is easier to get from Windows.

Practically, this means you will have to keep a backup of the public and
corresponding private key in case of a disaster, failing that you would have to
make a new release of the Windows and Linux client including the new public key.
As we use ed25519, backup `/etc/ssh/ssh_host_ed25519_key` and
`/etc/ssh/ssh_host_ed25519_key.pub` and keep it in a secure place.

Then, you have to get the server's public key in a format that the SSH client
will understand. We will do that later when preparing the clients.


# Client's versionning scheme

Publishing the client software requires to use a proper versionning scheme. To
deal with our specific use case (this project provides some files, you provide
others), I recommend you use two numbers separated by a `.`:

- the RemoteHelpBuilder version,
- the version of your settings.

So for the first time you build the package, the version will be `1.1`. If then
you change some string or your icon, the version will be `1.2`. And then if a
new RemoteHelpBuilder's version is published, the version would be `2.2`.


# Common clients prerequisites

You first need to choose a custom and unique software name, so that the helped:

- understands that only you can make use of this tool to help him,
- can eventually install more than one software like yours.

This documentation assumes that this name is "My remote help tool". Replace it
with the name you chose everytime you see it in this documentation.

You then need to find the name that will be used for the program and package
names. The best is to use your custom name chosen above but written in a
"computer friendly" way. In this documentation we will use
`my-remote-help-tool`. Replace it with the name you chose everytime you see it
in this documentation.


# Linux client

## Prepare the "upstream" release

### Prerequisites

Create a directory that will contain all your custom files in it. It is easier
to use the "computer friendly" name chosen above, so in this documentation we
will use `~/my-remote-help-tool/`. It is advised that you create it in a VCS to
keep track of your changes.


### Software icon

This is optionnal, but you can provide your own icon. If you do, I recommend
that you use the computer friendly name chosen above, so this examples uses
"my-remote-help-tool".

You can provide one or many sizes so that the desktop environment can pick the
best size for the user. You can have a look at what sizes and formats are
usually provided on Debian and derivatives by looking in `/usr/share/icons/`. If
you target Gnome, you should probably read the
[Gnome guidelines](https://developer.gnome.org/hig/3.32/icons-and-artwork.html.en).

If you want to provide an icon, you can put it in the following layout depending
on the sizes:

```
~/my-remote-help-tool/icons/
  ├── 48x48
  │   └── my-remote-help-tool.png
  ├── 256x256
  │   └── my-remote-help-tool.png
  ├── 512x512
  │   └── my-remote-help-tool.png
  └── scalable
      └── my-remote-help-tool.svgz
```


### Desktop entry

The desktop entry file will make your software appear in the helped desktop
menu. This file will be saved in
`~/my-remote-help-tool/my-remote-help-tool.desktop` and can be based on this
example:

```
[Desktop Entry]
Name=My remote help tool
Name[fr]=Mon outil d'aide à distance
Comment=Enable remote help
Comment[fr]=Active l'aide à distance
Exec=my-remote-help-tool
Terminal=false
Categories=Utility
Type=Application
Icon=my-remote-help-tool
StartupNotify=true
```

Be carefull to customize:

- The `Name`(s) and `Comment`(s) to match what the helped needs to see.
- The `Exec` line to match your Debian package name.
- The `Icon` line :
  - to match the name of your icon,
  - or to match the name of an icon that is already on the helped computer,
    probably in `/usr/share/icons/`.

Do not hesitate to read the
[desktop entry specification](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html)
if you have any question regarding this file.


### Software, strings and parameters

First copy the client software/script to include it in your upstream release
(remember to replace `my-remote-help-tool` by the name you chose previously):

```
cp ~/RemoteHelpBuilder/Linux-client/linux-client ~/my-remote-help-tool/my-remote-help-tool
```

Then copy the template containing variables that the client script will use:

```
cp ~/RemoteHelpBuilder/Linux-client/variables ~/my-remote-help-tool/variables
```

You must now edit `~/my-remote-help-tool/variables` to suit your
needs. All variables in this shell script will be used to feed the main script,
`linux-client`. As usual in shell scripts, be very careful to quote variables
and to escape double quotes with backslashes (`\"`). You can use `\n` or `\t` to
print line return or tabulation respectively.

Ensure these two files have execute rights.


### Intermediate server's SSH public key

As said earlier, we also include the server's public key, ed25519 in this
example. In the following command, be sure to use the IP address of you server
or its FQDN exactly the same way that you wrote it in
`~/my-remote-help-tool/linux-strings.sh` (otherwise the client will not use it):

```
$ ssh-keyscan -t ed25519 <you server's FQDN or IP address> > ~/my-remote-help-tool/ssh-server-public-key
```

If you changed the port SSH listens on, use the `-p` option.

For more information on this file format see `SSH_KNOWN_HOSTS FILE FORMAT`
section in `man 8 sshd`.


## First build of the Debian and derivatives package

Below instructions are very simple: do not hesitate to read the dedicated
chapter of the excellent
[Debian Administrator's Handbook](https://debian-handbook.info/browse/stable/sect.building-first-package.html)
for a step-by-step initiation or the
[official Debian documentation](https://www.debian.org/doc/manuals/maint-guide)
for full details. Please also understand that I have no particular experience
with Debian packaging: it works but it should certainely be improved.

Building a Debian package requires that you install `build-essential` and is
made easier by using tools from `dh-make` and `devscripts`:

```
# apt install build-essential dh-make devscripts
```

Go inside `~/my-remote-help-tool/` and generate all files needed for proper
packaging. Modify the following command to match:

- your name/company
- your package name (still "my-remote-help-tool" in our example)
- the "upstream" version number (for example "1.1" if RemoteHelpBuilder is in
  version 1 and if you publish it for the first time)
- your email address

```
cd ~/my-remote-help-tool/ && \
DEBFULLNAME="<your name>" dh_make \
  --packagename my-remote-help-tool_<upstream version> \
  --copyright gpl3 \
  --email <your email address> \
  --indep
  --createorig
```

Many files useless for this simple package have been created in the `debian/`
subdirectory, so we delete them:

```
$ cd debian && rm manpage.* menu.ex postinst.ex postrm.ex preinst.ex prerm.ex README.Debian README.source *.cron.d.ex *.doc-base.EX *-docs.docs watch.ex
```

You now need to edit some of the remaining files:

- `changelog`: modify the changelog of the initial release
- `compat`: do not modify
- `control`:
  - edit homepage and description (each line of the long description must begin
    with a space)
  - replace line `Depends: ${misc:Depends}` by `Depends: openssh-client, sshpass, x11vnc, yad, gnome-icon-theme`
- `copyright`: add the source and copyright for all the files (normally you
  would have to give credits to this project: this is great if you do so, but
  currently I do not require it)
- `rules`: do not modify
- `source/format`: do not modify

Still in the new `debian/` subdirectory, create a file called
`my-remote-help-tool.install` that defines where all files must be installed to.
Do not forget to modify it:

```
my-remote-help-tool                         usr/bin/
my-remote-help-tool.desktop                 usr/share/applications/
variables                                   usr/share/my-remote-help-tool/
ssh-server-public-key                       usr/share/my-remote-help-tool/
icons/48x48/my-remote-help-tool.png         usr/share/icons/hicolor/48x48/apps/
icons/128x128/my-remote-help-tool.png       usr/share/icons/hicolor/128x128/apps/
icons/256x256/my-remote-help-tool.png       usr/share/icons/hicolor/256x256/apps/
icons/512x512/my-remote-help-tool.png       usr/share/icons/hicolor/512x512/apps/
icons/scalable/my-remote-help-tool.svgz     usr/share/icons/hicolor/scalable/apps/
```

Finaly, go back to `~/my-remote-help-tool/` and build the package:

```
$ dpkg-buildpackage --unsigned-source --unsigned-changes
```

Resulting package and associated files (useless for us) are created in the
parent directory. Just test it and you are now ready to help Debian and
derivatives users!

Note: If your icon does not appear when running the client, see
(this entry)[#icon-is-not-displayed-when-running-the-linux-client] of the FAQ.


## Updating the Debian and derivatives package

### When you need to fix packaging

If you change any file in `~/my-remote-help-tool/debian` you need to rebuild the
package. `cd` into `~/my-remote-help-tool/` and use the following command to
edit the changelog. Be careful to increment the Debian revision (the number
after the `-`), `1.1-2` in this example:

```
$ DEBFULLNAME=<your name> DEBEMAIL=<your email> debchange --newversion 1.1-2 --preserve
```

Finalize the changelog:

```
$ DEBFULLNAME=<your name> DEBEMAIL=<your email> debchange --release ""
```

And rebuild the package:

```
$ dpkg-buildpackage --unsigned-source --unsigned-changes
```


### For everything else

When there is a new version of RemoteHelpBuilder that you want to use, or if you
need to change one of your files (`variables`, `ssh-server-public-key` or
`my-remote-help-tool.desktop`), you will have to build a new package.

Make the changes in `~/my-remote-help-tool/`. Do not forget to adapt
`~/my-remote-help-tool/debian/` files if needed.

Define what will be the new version number of your package:

- If you include some RemoteHelpBuilder changes, increase the first number,
- If you modify one of your files, increase the second number,
- In all cases, reset the Debian revision to `1`.

Edit the changelog, specifying the version (`1.2-1` in this example):

```
$ DEBFULLNAME=<your name> DEBEMAIL=<your email> debchange --newversion 1.2-1 --preserve
```

Finalize the changelog:

```
$ DEBFULLNAME=<your name> DEBEMAIL=<your email> debchange --release ""
```

Create a tarball in the upper directory to simulate the upstream release
(otherwise dpkg-buildpackage will not work), with a filename ending with
`_<version>.orig.tar.gz` (note that the version must not include the Debian
revision):

```
$ tar -caf ../my-remote-help-tool_1.2.orig.tar.gz .
```

And rebuild the package:

```
$ dpkg-buildpackage --unsigned-source --unsigned-changes
```


# Windows client

## First build

### Prerequisites

This requires the use of a Windows computer.

Create a directory that will contain all your custom files in it. It is easier
to use your "computer friendly" software name chosen above, so in this
documentation we will use `my-remote-help-tool\`. It is advised that you create
it in a VCS to keep track of your changes.

Also, ensure you have access to files inside the `Windows-client` directory of
RemoteHelpBuilder.


### PuTTY

From the
[PuTTY website](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html),
download the 32 bits version of `plink.exe` and save it to
`my-remote-help-tool\Sources\PuTTY\`.

Download PuTTY Unix source archive and extract the `LICENCE` file to
`my-remote-help-tool\Sources\PuTTY\`.

Create in this same directory a file called `version.txt` and write inside the
version (for example `0.71`), so that you and the user can easily retrieve it.


### UltraVNC

From the [UltraVNC website](http://www.uvnc.com), download the 32 bits version
and save it anywhere on a Windows computer. Run the installer, which should be
called something like `UltraVNC_1_2_24_X86_Setup.exe`. When choosing components
to install, you can choose only `UltraVNC Server`. Do not register the system
service.

From the installation target (probably `C:\Program Files (x86)\uvnc bvba\`),
copy `UltraVNC\` directory to `my-remote-help-tool\Sources\`. Then, replace the
settings file `my-remote-help-tool\Sources\UltraVNC\ultravnc.ini` by the one
provided in the `Windows-client\` directory of RemoteHelpBuilder.

Optionnal: If you want to generate yourself a proper `ultravnc.ini`:

- Run the newly installed `UltraVNC server settings` application.
- In the `Network` tab, uncheck `Enable Java port` and check `LoopbackOnly`.
- In the `Security` tab, define the two passwords (I could not make it work
  without password or with a blank password). The `ultravnc.ini` file provided
  by RemoteHelpBuilder has password sets to `foo`.
- Click `OK` to generate your `ultravnc.ini`

Notes:

- Some of the UltraVNC files are certainely not needed, but I do not know which
  ones.
- This step might be possible under Wine but running the installer is not
  straightforward.


### Wrapper script and variables

Putty (and so plink.exe) does not use the same format as OpenSSH does for SSH
public keys. To get your intermediate server's public key from Windows (or from
Linux using Wine), run:

```
plink.exe -v -batch -P <ssh port> <FQDN or IP address>
```

The variable that will be used is the line that looks like
`ssh-ed25519 255 e5:32:8f:c3:fb:c6:e3:6e:8a:45:78:dc:d8:5c:84:03`.

If you chose a signature algorithm other than ed25519, like RSA, you can get the
same line from a Linux computer by converting the _known host_ file used for
building the Linux client:

```
$ printf "ssh-rsa "; ssh-keygen -l -E md5 -f ~/my-remote-help-tool/ssh-server-public-key | sed 's/MD5://' | cut -d " " -f -2
```

Copy `Windows-client\RemoteHelp.vbs` to `my-remote-help-tool\Sources\`. Edit the
variables at the beginning to suit your needs. For those who do not know
VBScript, see example values for quoting and line returns.

Also, do not hesitate to add your copyright at the beginning of the file.


### License and changelog

Copy `Windows-client\License.txt` and `Windows-client\Changelog.txt` to
`my-remote-help-tool\Sources\`.

Modify them to have a proper changelog and eventually to add your copyright in
the license file.


### Software icon

If you want, you can provide you own icon for the software: it will be used by
the installer and for the desktop and application menu shortcuts. Personnally
I use GIMP to export my logo in 256x256 px in the .ico format. Save it in
`my-remote-help-tool\Sources\`.


### Building with InnoSetup

To create the Windows installer, you will have to download the Windows libre
software [Inno Setup](http://www.jrsoftware.org/isinfo.php). Of course, you can
run it on Windows, but it also works well under Wine (tested with version 4.0).
Here we use installer `innosetup-qsp-5.6.1-unicode.exe` but I suppose it should
work with any other version. Let Inno Setup installer install also the "Inno
Setup Preprocessor".

Copy `Windows-client\InnoSetup.iss` to `my-remote-help-tool\`, and modify it
with InnoSetup to suit your needs:

- Generate a new application GUID by going in "Tools -> Generate GUID" and use
  it to replace the one provided in the example.
- Modify variables in the `[Setup]` section (do not forget line starting with
  `#define` if you changed the icon name).
- Eventually add or removes languages in the `[Languages]` section.

Note that all encodings are not supported: for example, on my setup, I must use
ISO-8859-15 (Windows-1252) and not UTF-8.

Once done, click on the "Compile" button at the top of the window and your
installer will appear in `my-remote-help-tool\Output\`. Test the installer
(works well under Wine) and your program (which obviously needs Windows).


## Building a new client version

When a new version of RemoteHelpBuilder improves the VBScript (fortunately it
will happen!), when PuTTY or UltraVNC release a security update, or when you
want to improve the InnoSetup installer, you need to build a new client version.

- Update files that need it in `my-remote-help-tool\Sources\`.
- Explain what changes in `Windows-client\Changelog.txt`.
- Edit `my-remote-help-tool\InnoSetup.iss` to eventually increase `AppVersion`
- Compile the installer with InnoSetup.


# Usage

Finally the easy part!

1. First time you help someone, explain to the helped how to install your client
   software.
2. Connect to the intermediate server with X forwarding and eventually
   compression (`-C`, if connection from the intermediate server to your
   computer is too slow): `ssh -XC <username>@<intermediate_server> -p <ssh_port>`
3. Generate a random password: `sudo remote-help-toggle enable`
4. Give this password to the helped so that he can type it in the client
   software.
5. Start the VNC client (command line was written when running the previous
   command), and provide help!
3. When done, disable the remote-help user: `sudo remote-help-toggle disable`


# Release policy

A new RemoteHelpBuilder release is published when commits modify one of the
provided files of Windows or Linux client (except when modification is only on
examples, comments or formatting). No release is published when documentation is
modified.


# Frequently asked questions

## Debugging

### On the intermediate server

You can check what OpenSSH server logs about the SSH connection in
`/var/log/auth.log`. If the SSH connection is successful, you can monitor if the
VNC server is listening with `lsof -i | grep 5900` (as root).


### On the Linux client

You can try to run the client from command line with verbose output:

```
sh -x /usr/bin/my-remote-help-tool
```

### On the Windows client

While running the client, open `%TEMP%` (usually
`C:\Users\username\AppData\Local\Temp\`) and read the content of the files
called `plink-stdout.log` and `plink-stderr.log`.

Once the SSH connection established, you can also check if the UltraVNC server
is running, either in the application tray or using the task manager.


## Icon is not displayed when running the Linux client

If your home directory contains a directory with the exact same name of
your icon, your icon will not appear where running the client software: this is
because the prompt is done with Yad, which uses GTK, which tries to use the
folder as the icon.


## How to run the VNC client on your computer and not on the server

By default, OpenSSH server does not expose on the network the ports forwarded by
clients. This is very good thing! If you want to be able to run the VNC client
on your computer and not on the server, you need to add the following line in
`/etc/ssh/sshd_config`:

```
Match User remote-help
    …
    GatewayPorts yes
    …
```

Warning: As there is no VNC password, be very careful to properly restrict
access to TCP port 5900 on your server. If you do not know how to do this, do
not use this setting!


# License

Copyright (C) 2019 Yvan Masson, https://masson-informatique.fr,
yvan@masson-informatique.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

